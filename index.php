<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title> Data - data.ayadanconlangs.com </title>
  <meta name="description" content="Computer-readable conlang data for use to build APIs and other products">
  <meta name="author" content="Rachel Singh">

  <link rel="stylesheet" href="basic.css">

</head>

<body>
    <h1> Data @ ayadanconlangs.com </h1>
    
    <p><a href="http://api.ayadanconlangs.com">API</a> | <a href="http://data.ayadanconlangs.com">Data</a> | <a href="http://tools.ayadanconlangs.com">Tools</a> | <a href="http://www.ayadanconlangs.com">Main website</a></p>
    
    <p>
        This page provides access to various conalng data resources
        (such as dictionaries) that are computer-readable, making the
        data ideal for use in making new programs, websites, APIs, etc.
    </p>
    
    <hr>
    
    <h2>Ido</h2>
    
    <table>
        <tr>
            <td>Esperanto &#8596; English dictionary<br>From <a href="http://www.denisowski.org/Esperanto/ESPDIC/espdic_readme.html">Paul Denisowski</a>, CC-By3</td>
                <td><a href="esperanto/eo-to-en_espdic.txt">eo-to-en_espdic.txt</a></td> <!-- CSV -->
                <!-- <td><a href="">CSV</a></td> --> <!-- CSV -->
                <!-- <td><a href="">Json</a></td> --> <!-- Json -->
        </tr>
    </table>
    
    <h2>Ido</h2>
    
    <table>
        <tr>
            <td>Deutsch &#8596; Ido dictionary<br>From kanaria1973.ido.li</td>
                <td><a href="ido/de-to-ido.txt">de-to-ido.txt</a></td> <!-- CSV -->
                <td><a href="ido/ido-to-de.txt">ido-to-de.txt</a></td> <!-- CSV -->
                <!-- <td><a href="">CSV</a></td> --> <!-- CSV -->
                <!-- <td><a href="">Json</a></td> --> <!-- Json -->
        </tr>
        
        <tr>
            <td>English &#8596; Ido dictionary<br>From idolinguo.org.uk</td>
                <td><a href="ido/en-to-ido.txt">en-to-ido.txt</a></td> <!-- CSV -->
                <td><a href="ido/ido-to-en.txt">ido-to-en.txt</a></td> <!-- CSV -->
                <!-- <td><a href="">CSV</a></td> --> <!-- CSV -->
                <!-- <td><a href="">Json</a></td> --> <!-- Json -->
        </tr>
        
        <tr>
            <td>Español &#8596; Ido dictionary<br>From kanaria1973.ido.li</td>
                <td><a href="ido/es-to-ido.txt">es-to-ido.txt</a></td> <!-- CSV -->
                <td><a href="ido/ido-to-es.txt">ido-to-es.txt</a></td> <!-- CSV -->
                <!-- <td><a href="">CSV</a></td> --> <!-- CSV -->
                <!-- <td><a href="">Json</a></td> --> <!-- Json -->
        </tr>
        
        <tr>
            <td>Esperanto &#8596; Ido dictionary<br>From kanaria1973.ido.li</td>
                <td><a href="ido/eo-to-ido.txt">eo-to-ido.txt</a></td> <!-- CSV -->
                <td><a href="ido/ido-to-eo.txt">ido-to-eo.txt</a></td> <!-- CSV -->
                <!-- <td><a href="">CSV</a></td> --> <!-- CSV -->
                <!-- <td><a href="">Json</a></td> --> <!-- Json -->
        </tr>
        
        <tr>
            <td>Français &#8596; Ido dictionary<br>From kanaria1973.ido.li</td>
                <td><a href="ido/ido-to-fr.txt">ido-to-fr.txt</a></td> <!-- CSV -->
                <!-- <td><a href="">CSV</a></td> --> <!-- CSV -->
                <!-- <td><a href="">Json</a></td> --> <!-- Json -->
        </tr>
        
        <tr>
            <td>Italiano &#8596; Ido dictionary<br>From kanaria1973.ido.li</td>
                <td><a href="ido/ido-to-it.txt">ido-to-it.txt</a></td> <!-- CSV -->
                <!-- <td><a href="">CSV</a></td> --> <!-- CSV -->
                <!-- <td><a href="">Json</a></td> --> <!-- Json -->
        </tr>
        
        <tr>
            <td>Nederlands &#8596; Ido dictionary<br>From kanaria1973.ido.li</td>
                <td><a href="ido/ido-to-nl.txt">ido-to-nl.txt</a></td> <!-- CSV -->
                <!-- <td><a href="">CSV</a></td> --> <!-- CSV -->
                <!-- <td><a href="">Json</a></td> --> <!-- Json -->
        </tr>
        
        <tr>
            <td>日本語 &#8596; Ido dictionary<br>From kanaria1973.ido.li</td>
                <td><a href="ido/jp-to-ido.txt">jp-to-ido.txt</a></td> <!-- CSV -->
                <td><a href="ido/ido-to-jp.txt">ido-to-jp.txt</a></td> <!-- CSV -->
                <!-- <td><a href="">CSV</a></td> --> <!-- CSV -->
                <!-- <td><a href="">Json</a></td> --> <!-- Json -->
        </tr>
        
        <tr>
            <td>Portugues &#8596; Ido dictionary<br>From kanaria1973.ido.li</td>
                <td><a href="ido/ido-to-pt.txt">ido-to-pt.txt</a></td> <!-- CSV -->
                <!-- <td><a href="">CSV</a></td> --> <!-- CSV -->
                <!-- <td><a href="">Json</a></td> --> <!-- Json -->
        </tr>
        
        <tr>
            <td>русский &#8596; Ido dictionary<br>From kanaria1973.ido.li</td>
                <td><a href="ido/ido-to-ru.txt">ido-to-ru.txt</a></td> <!-- CSV -->
                <!-- <td><a href="">CSV</a></td> --> <!-- CSV -->
                <!-- <td><a href="">Json</a></td> --> <!-- Json -->
        </tr>
        
        <tr>
            <td>Suomi &#8596; Ido dictionary<br>From kanaria1973.ido.li</td>
                <td><a href="ido/ido-to-fi.txt">ido-to-fi.txt</a></td> <!-- CSV -->
                <!-- <td><a href="">CSV</a></td> --> <!-- CSV -->
                <!-- <td><a href="">Json</a></td> --> <!-- Json -->
        </tr>
        
    </table>
    
    <hr>
    
    <h2>Láadan</h2>
    
    <table>
        <tr>
            <td>English &#8596; Láadan dictionary</td>
                <td><a href="laadan/dictionary_laadan-to-english_living.json">dictionary_laadan-to-english_living.json</a></td> <!-- Json -->
                <td><a href="laadan/laadan-to-english_2018-06-21.json">laadan-to-english_2018-06-21.json</a></td> <!-- Json -->
                <td><a href="laadan/laadan-to-english_2018-06-21.csv">laadan-to-english_2018-06-21.csv</a></td> <!-- CSV -->
        </tr>
        
        <tr>
            <td>Español &#8596; Láadan dictionary<br>By Tea Duckie</td>
                <td><a href="laadan/laadan_to_spanish.csv">laadan_to_spanish.csv</a></td> <!-- CSV -->
        </tr>
    </table>
    
    
</body>
</html>
